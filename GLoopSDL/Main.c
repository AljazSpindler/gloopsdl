#include <stdio.h>
#include <SDL.h>
#include "constants.h"

int game_is_running = FALSE;
int game_playing = FALSE;
SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;

int last_frame_time = 0;

struct rectangle
{
  float x;
  float y;
  float width;
  float height;
  float velocity_x;
  float velocity_y;
} ball, paddle;

int initialize_window(void)
{
  if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
  {
    fprintf(stderr, "Error init SDL.\n");
    return FALSE;
  }

  window = SDL_CreateWindow(
    NULL, 
    SDL_WINDOWPOS_CENTERED, 
    SDL_WINDOWPOS_CENTERED, 
    WINDOW_WIDTH, 
    WINDOW_HEIGHT,
    SDL_WINDOW_BORDERLESS
    );

  if (!window)
  {
    fprintf(stderr, "Error creating SDL window.\n");
    return FALSE;
  }

  renderer = SDL_CreateRenderer(window, -1, 0);

  if (!renderer)
  {
    fprintf(stderr, "Error creating SDL renderer.\n");
    return FALSE;
  }

  return TRUE;
}

void destroy_window()
{
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();
}

void setup()
{
  ball.x = 20;
  ball.y = 20;
  ball.width = 15;
  ball.height = 15;
  ball.velocity_x = BALL_SPEED;
  ball.velocity_y = BALL_SPEED;

  paddle.x = 0;
  paddle.y = WINDOW_HEIGHT - 25;
  paddle.width = 80;
  paddle.height = 20;
  paddle.velocity_x = 0;
  paddle.velocity_y = 0;
}

void process_input()
{
  SDL_Event ev;
  SDL_PollEvent(&ev);

  switch (ev.type)
  {
  case SDL_QUIT:
    game_is_running = FALSE;
    break;
  case SDL_KEYDOWN:
    if (ev.key.keysym.sym == SDLK_ESCAPE)
    {
      game_is_running = FALSE;
    }
    else if (ev.key.keysym.sym == SDLK_LEFT)
    {
      paddle.velocity_x -= PADDLE_SPEED;
    }
    else if (ev.key.keysym.sym == SDLK_RIGHT)
    {
      paddle.velocity_x += PADDLE_SPEED;
    }
    else if (ev.key.keysym.sym == SDLK_RETURN)
    {
      game_playing = TRUE;
      setup();
    }
    break;
  default:
    break;
  }
}

void update()
{
  int time_to_wait = FRAME_TARGET_TIME - (SDL_GetTicks() - last_frame_time);
  if (time_to_wait > 0 && time_to_wait <= FRAME_TARGET_TIME)
  {
    SDL_Delay(time_to_wait);
  }

  float delta_time = (SDL_GetTicks() - last_frame_time) / 1000.0f;
  last_frame_time = SDL_GetTicks();

  if (game_playing)
  {

    // Ball checks and processing.

    float ball_mov_x = ball.velocity_x * delta_time;
    float ball_mov_y = ball.velocity_y * delta_time;

    if (ball.x + ball.width + ball_mov_x >= WINDOW_WIDTH)
    {
      ball.velocity_x = -BALL_SPEED;
      ball_mov_x = ball.velocity_x * delta_time;
    }
    else if (ball.x + ball_mov_x <= 0)
    {
      ball.velocity_x = BALL_SPEED;
      ball_mov_x = ball.velocity_x * delta_time;
    }

    if (ball.y + ball.height + ball_mov_y >= WINDOW_HEIGHT)
    {
      // GAME OVER
      game_playing = FALSE;
    }
    else if (ball.y + ball_mov_y <= 0)
    {
      ball.velocity_y = BALL_SPEED;
      ball_mov_y = ball.velocity_y * delta_time;
    }

    // TODO: Paddle block check.

    ball.x += ball_mov_x;
    ball.y += ball_mov_y;


    // Paddle check and processing.

    float paddle_mov_x = paddle.velocity_x * delta_time;
    float paddle_mov_y = paddle.velocity_y * delta_time;

    // Check if we can move - do not go over the edge.
    if (paddle.x + paddle.width + paddle_mov_x <= WINDOW_WIDTH && paddle.x + paddle_mov_x >= 0)
    {
      paddle.x += paddle_mov_x;
    }
    // Does not move in y so no check is really needed.
    paddle.y += paddle.velocity_y * delta_time;

    // Reset paddle velocity.
    paddle.velocity_x = 0;
    paddle.velocity_y = 0;
  }
}

void render()
{
  SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
  SDL_RenderClear(renderer);

  SDL_Rect new_ball = { (int)ball.x, (int)ball.y, (int)ball.width, (int)ball.height };
  SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
  SDL_RenderFillRect(renderer, &new_ball);

  SDL_Rect new_paddle = { (int)paddle.x, (int)paddle.y, (int)paddle.width, (int)paddle.height };
  SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
  SDL_RenderFillRect(renderer, &new_paddle);

  SDL_RenderPresent(renderer); // Buffer swap - back to front.
}

int main(int argc, char* args[])
{
  game_is_running = initialize_window();

  setup();

  game_playing = TRUE;

  while (game_is_running)
  {
    process_input();
    update();
    render();
  }

  destroy_window();

  return 0;
}